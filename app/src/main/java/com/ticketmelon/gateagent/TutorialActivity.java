package com.ticketmelon.gateagent;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.augustopicciani.drawablepageindicator.widget.DrawablePagerIndicator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TutorialActivity extends AppCompatActivity {
    ImageView lnkSkipTutorial;

    MyPageAdapter pageAdapter;

    boolean already_logged_in = false;

    int MY_PERMISSIONS_REQUEST_Camera = 101;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        lnkSkipTutorial = (ImageView)findViewById(R.id.lnkSkipTutorial);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CAMERA)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST_Camera);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }

        Intent i = getIntent();
        if(i != null && i.getExtras() != null) {
            already_logged_in = true;
           Log.d("GateAgent", "You are already logged in");
            Map<String, Integer> map = new HashMap<String, Integer>();
            map.put("done", R.drawable.done_button_1point5x);
            lnkSkipTutorial.setImageResource(map.get("done"));
            lnkSkipTutorial.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    finish();
                }
            });
        } else {
            Log.d("GateAgent", "You need to log in");
            lnkSkipTutorial.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(TutorialActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            });
        }

        /*
        lnkSkipTutorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(TutorialActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        });
        */
        List<Fragment> fragments = getFragments();
        pageAdapter = new MyPageAdapter(getSupportFragmentManager(), fragments);
        ViewPager pager = (ViewPager)findViewById(R.id.viewpager);

        pager.setAdapter(pageAdapter);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Log.d("GateAgent", String.valueOf(position) + " page selected.");
                if(already_logged_in == true) {

                } else {
                    if(position == 8) {
                        Log.d("GateAgent", "Display Start Image");
                        Map<String, Integer> map = new HashMap<String, Integer>();
                        map.put("start", R.drawable.start_button_1point5x);
                        lnkSkipTutorial.setImageResource(map.get("start"));
                    } if(position != 8) {
                        Map<String, Integer> map = new HashMap<String, Integer>();
                        map.put("skip", R.drawable.skip_resize_button_1point5x);
                        lnkSkipTutorial.setImageResource(map.get("skip"));
                    }
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        DrawablePagerIndicator drawablePagerIndicator = (DrawablePagerIndicator)findViewById(R.id.drawableIndicator);
        drawablePagerIndicator.setViewPager(pager);

        //mIndicator = (CirclePageIndicator)findViewById(R.id.circleIndicator);
        //mIndicator.setViewPager(pager);


        // Set a toolbar to replace the actionbar.
        Toolbar tutorial_toolbar = (Toolbar)findViewById(R.id.tutorial_toolbar);
        setSupportActionBar(tutorial_toolbar);



    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 101 : {
                Log.i("aaa","aaa");
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
    private List<Fragment> getFragments() {
        List<Fragment> fList = new ArrayList<Fragment>();
        fList.add(MyTutorialFragment.newInstance(R.drawable.tutorial1));
        fList.add(MyTutorialFragment.newInstance(R.drawable.tutorial2));
        fList.add(MyTutorialFragment.newInstance(R.drawable.tutorial3));
        fList.add(MyTutorialFragment.newInstance(R.drawable.tutorial4));
        fList.add(MyTutorialFragment.newInstance(R.drawable.tutorial5));
        fList.add(MyTutorialFragment.newInstance(R.drawable.tutorial6));
        fList.add(MyTutorialFragment.newInstance(R.drawable.tutorial7));
        fList.add(MyTutorialFragment.newInstance(R.drawable.tutorial8));
        fList.add(MyTutorialFragment.newInstance(R.drawable.tutorial9));
        return fList;
    }
}
