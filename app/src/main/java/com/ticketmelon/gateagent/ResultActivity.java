package com.ticketmelon.gateagent;

import android.content.Intent;
import android.service.media.MediaBrowserService;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ResultActivity extends AppCompatActivity {

    List<Ticket> ticketList;
    int used = 0;
    int fail = 0;
    int success = 0;
    ExpandListAdapter listAdapter;

    private ArrayList<ExpandListGroup> ExpListItems;

    List<Ticket> listDataHeader;
    List<Ticket> listDataChild;

    Map<String, List<Ticket>> ticketCollection;
    ExpandableListView resultList;

    ImageButton imageButton;
    DrawerLayout mDrawerLayout;

    DatabaseHandler db;

    EditText inputSearch;

    Session sessionData;

    String myToken;

    ImageView imgResultDropdown;

    ImageButton btnHome, btnValidate;

    Button btnSearch;

    TextView txtTutorial, txtLogout, txtScanned, txtScanned2 ,txtUsed,txtFail,txtSuccess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        Intent i = getIntent();
        if(i != null && i.getExtras() != null) {
            sessionData = (Session)i.getSerializableExtra("sessionObj");
            myToken = i.getExtras().getString("token");
        }
        Log.i(this.getLocalClassName()+"LOG", myToken);
        imgResultDropdown = (ImageView)findViewById(R.id.imgResultDropdown);

        txtScanned = (TextView) findViewById(R.id.scanned);

        txtScanned2 = (TextView) findViewById(R.id.scanned2);



        btnHome = (ImageButton)findViewById(R.id.btnHome);

        btnHome.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                Intent i = new Intent(ResultActivity.this, MyEventActivity.class);
                i.putExtra("sessionObj", sessionData);
                i.putExtra("token", myToken);
                startActivity(i);
            }
        });

        txtTutorial = (TextView)findViewById(R.id.txtTutorial);
        txtTutorial.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ResultActivity.this, TutorialActivity.class);
                i.putExtra("sessionObj", sessionData);
                i.putExtra("token", myToken);
                startActivity(i);
                // finish();
            }
        });

        txtLogout = (TextView)findViewById(R.id.txtLogout);
        txtLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ResultActivity.this, LoginActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                finish();
            }
        });

        btnValidate = (ImageButton)findViewById(R.id.btnValidate);
        btnValidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(ResultActivity.this, ValidateActivity.class);
                i.putExtra("sessionObj", sessionData);
                i.putExtra("token", myToken);
                startActivity(i);
            }
        });

        imageButton = (ImageButton)findViewById(R.id.imageButton);

        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        resultList = (ExpandableListView)findViewById(R.id.resultList); // Get the listview

        // preparing list data
        prepareListData();



        listAdapter = new ExpandListAdapter(ResultActivity.this, ExpListItems);

        resultList.setAdapter(listAdapter);


        resultList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                int index = groupPosition + 1;
                Log.d("GateAgent", "Position " + groupPosition + " expanded.");


            }
        });

        resultList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {

                Log.d("GateAgent", "Position " + groupPosition + " collapsed.");
            }
        });

        resultList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                Log.d("GateAgent", "Child of " + groupPosition  + " clicked");
                parent.collapseGroup(groupPosition);
                return false;
            }
        });

        inputSearch = (EditText)findViewById(R.id.inputSearch);
        inputSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_DONE){
                    loadData(inputSearch.getText().toString(),myToken);
                    return true;
                }
                return false;
            }
        });

        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.d("GateAgent", "You typed " + s);
                ResultActivity.this.listAdapter.getFilter().filter(s);
                prepareListData();

                listAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {


            }

        });


        btnSearch = (Button) findViewById(R.id.btSearch);

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData(inputSearch.getText().toString(),myToken);
            }
        });


    }



    private void prepareListData() {
        //db = new DatabaseHandler(this);
        //db.addTicket(new Ticket("V6E4R1","Early Bird","15:35:00","Scan Failed","15/09/16","4007","lee","SOL"));
        ExpListItems = new ArrayList<ExpandListGroup>();


        /**
        listDataHeader = new ArrayList<Ticket>();
        listDataChild = new ArrayList<Ticket>();
        ticketCollection = new LinkedHashMap<String, List<Ticket>>();
        **/

        DatabaseHandler db = new DatabaseHandler(this);
        ticketList = db.getAllTickets(sessionData.getUsername());
        txtScanned.setVisibility(View.VISIBLE);
        txtScanned.setText("Total Tickets scanned: "+ticketList.size());

        if(ticketList.size() >= 0) {

            for(Ticket cn:ticketList) {
                Log.d("GateAgent", cn.getCode());
                ExpandListGroup group = new ExpandListGroup();
                group.setCode(cn.getCode());
                group.setIssue(cn.getStatus());
                if (cn.getStatus().toString().contains("Success")) {
                    success++;
                }else if (cn.getStatus().toString().contains("Used")) {
                    used++;
                }else{
                    fail++;
                }


                group.setScanned_time(cn.getTime_scanned());
                group.setType(cn.getType());
                ExpandListChild ch = new ExpandListChild();
                ch.setOrder_id(cn.getOrderNumber());
                ch.setScanned_time(cn.getTime_scanned());
                ch.setScanned_date(cn.getDate_scanned());
                group.setItems(ch);

                ExpListItems.add(group);



                /**
                listDataHeader.add(cn);
                String log = "Code : " + cn.getCode();
                Log.d("GateAgent", log);
                listDataChild = new ArrayList<Ticket>();
                listDataChild.add(cn);
                ticketCollection.put(cn.getCode(), listDataChild);
                 **/


            }
            txtScanned2.setVisibility(View.VISIBLE);
            txtScanned2.setText("Success:  "+success+"  Used:  "+used+"  Failed:  "+fail+"");

            /*
            count = 0;
            for(Ticket cn:ticketList) {
                List<Ticket> tmp = new ArrayList<Ticket>();
                tmp.add(cn);
                listDataChild.put(listDataHeader.get(count).getCode(), tmp);
                count = count + 1;
            }
            */


        }

    }
    public void loadData(String orderid,String token){
        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
        RequestBody body = RequestBody.create(mediaType, "order_id="+orderid+"&token="+token);
        Request request = new Request.Builder()
                .url("https://www.ticketmelon.com/ga/ticket/search")
                .post(body)
                .addHeader("content-type", "application/x-www-form-urlencoded")
                .addHeader("cache-control", "no-cache")
                .build();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {



            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    JSONArray ticketArr = new JSONArray(response.body().string());
                    ExpListItems = new ArrayList<ExpandListGroup>();
                    Log.i("sdasdasd","sdfsdf   " + ticketArr.length());
                    if(ticketArr.length() > 0){
                        Log.i("sdasdasd","sdfsdf   " + ticketArr.length());
                        for(int i =0;i<ticketArr.length();i++){
                            Log.i("sdasdasd","sdfsdf   " + ticketArr.length() + " "+ i);
                                JSONObject ticket = ticketArr.getJSONObject(i);



                                ExpandListGroup group = new ExpandListGroup();
                                group.setCode(ticket.getString("code")+"");
                                group.setIssue(ticket.getString("status")+"");


                                String[] s = ticket.getString("scaned_at").split(" ");
                                group.setScanned_time(s[1]);
                                group.setType(ticket.getString("ticket_type_name")+"");
                                ExpandListChild ch = new ExpandListChild();
                                ch.setOrder_id(ticket.getString("order_id")+"");
                                ch.setScanned_time(s[1]);
                                ch.setScanned_date(s[0]);
                                group.setItems(ch);

                                ExpListItems.add(group);


                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                listAdapter.addSet(ExpListItems);
                                listAdapter.notifyDataSetChanged();
                            }
                        });
                    }else{

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });
    }



    private void loadChild(Ticket[] tickets) {
        listDataChild = new ArrayList<Ticket>();
    }


}
