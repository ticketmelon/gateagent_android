package com.ticketmelon.gateagent;

/**
 * Created by cube on 2/18/2016.
 */
public class Ticket {
    private int id;
    private String code;
    private String type;
    private String time_scanned;
    private String status;
    private String date_scanned;
    private String orderNumber;
    private String agentUser;
    private String eventName;

    public Ticket() {

    }

    public Ticket(String code, String type, String time, String status, String date_scanned, String orderNumber, String agentUser, String eventName) {
        this.code = code;
        this.type = type;
        this.time_scanned = time;
        this.status = status;
        this.date_scanned = date_scanned;
        this.orderNumber = orderNumber;
        this.agentUser = agentUser;
        this.eventName = eventName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTime_scanned() {
        return time_scanned;
    }

    public void setTime_scanned(String time_scanned) {
        this.time_scanned = time_scanned;
    }

    public String getAgentUser() {
        return agentUser;
    }

    public void setAgentUser(String agentUser) {
        this.agentUser = agentUser;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public String getTime() {
        return time_scanned;
    }

    public String getStatus() {
        return status;
    }

    public String getDate_scanned() {
        return date_scanned;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setTime(String time) {
        this.time_scanned = time;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setDate_scanned(String date_scanned) {
        this.date_scanned = date_scanned;
    }
}
