package com.ticketmelon.gateagent;

import android.Manifest;
import android.app.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;


import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import android.util.Log;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.ticketmelon.gateagent.camera.CameraSource;
import com.ticketmelon.gateagent.camera.CameraSourcePreview;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import java.net.HttpURLConnection;



import fr.castorflex.android.circularprogressbar.CircularProgressBar;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ValidateActivity extends AppCompatActivity {

    String scan_status = "";

    private static final String TAG = "Barcode-reader";

    public static final String AutoFocus = "AutoFocus";
    public static final String UseFlash = "UseFlash";
    public static final String BarcodeObject = "Barcode";

    private static final int RC_HANDLE_CAMERA_PERM = 2;

    CircularProgressBar cpbLoginProgress;

    Session sessionData;

    CameraSourcePreview cameraView;

    BarcodeDetector barcodeDetector;
    CameraSource cameraSource;

    Button btnTryAgain, btnManualValidate, btnNextScan , btnManualValidateRFID;

    ImageButton btnHome, btnResult;

    ImageButton imageButton;
    DrawerLayout mDrawerLayout;

    TextView txtQRCode, txtValidateStatusPrompt, txtTicketBought, txtFirstName, txtLastName, txtTicketType, txtScannedTime, txtScannedDate, txtOrderId, txtFailedOrderId, txtMySeat;
    ImageView imgValidateResult, imgManualValidate;

    LinearLayout validate_ticket_info, used_ticket_info, scan_failed_info, backgroundLayer, resultImageLayer, validBackgroundLayer;

    String myToken, barcode, purchased_datetime, buyer, buyer_lastname, ticket_type, msg, verify_at, verified_date, verified_time, order_id,user_id;
    String code;
    String scannedDate, scannedTime, seatNumber, seatRow, seatSection, mySeat;

    TextView txtTutorial, txtLogout;
    View ParentView;
    EditText edrfid;
    private Switch rfidSwitch;
    String coderfid = "";

    int delay = 3000;
    Boolean checkTap = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validate);

        Intent i = getIntent();
        if(i != null && i.getExtras() != null) {
            myToken = i.getExtras().getString("token");
            sessionData = (Session)i.getSerializableExtra("sessionObj");
        }

        cpbLoginProgress = (CircularProgressBar)findViewById(R.id.cpbLoginProgress);
        cpbLoginProgress.setIndeterminate(false);

        edrfid = (EditText) findViewById(R.id.editrfid);
        rfidSwitch = (Switch) findViewById(R.id.switch1);
        rfidSwitch.setVisibility(View.VISIBLE);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("Profile", MODE_PRIVATE);
        rfidSwitch.setChecked(pref.getBoolean("rfidUsed",false));
        btnManualValidateRFID = (Button)findViewById(R.id.btnManualValidateRFID);
        btnManualValidateRFID.setVisibility(View.INVISIBLE);
        btnManualValidateRFID.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ValidateActivity.this, ManualValidationActivity.class);
                i.putExtra("token", myToken);
                i.putExtra("sessionObj", sessionData);
                i.putExtra("RFID", ""+rfidSwitch.isChecked());
                startActivity(i);

            }
        });

        edrfid.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    Toast.makeText(ValidateActivity.this, edrfid.getText()+" ", Toast.LENGTH_SHORT).show();
                    cpbLoginProgress.setIndeterminate(true);
                    coderfid = edrfid.getText().toString().replaceAll("\\s+","");
                    edrfid.setVisibility(View.GONE);
                    edrfid.setText("");
                    OkHttpClient client = new OkHttpClient();

                    MediaType mediaType = MediaType.parse("multipart/form-data; boundary=---011000010111000001101001");
                    // RequestBody body = RequestBody.create(mediaType, "-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"qrcode\"\r\n\r\n"+code+"\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"rfid\"\r\n\r\n"+coderfid+"\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"balance\"\r\n\r\n0\r\n-----011000010111000001101001--");
                    RequestBody body = RequestBody.create(mediaType, "-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"qrcode\"\r\n\r\n"+code+"\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"token\"\r\n\r\n"+myToken+"\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"ticket_type_id\"\r\n\r\n\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"rfid\"\r\n\r\n"+coderfid+"\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"balance\"\r\n\r\n0\r\n-----011000010111000001101001--");
                    Request request = new Request.Builder()
                            .url("http://52.163.59.88//api/laravel/rfid/accounts/pair")
                            .post(body)
                            .addHeader("content-type", "multipart/form-data; boundary=---011000010111000001101001")
                            .addHeader("cache-control", "no-cache")
                            .build();

                    client.newCall(request).enqueue(new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    cpbLoginProgress.setIndeterminate(false);
                                    scan_status = "RFID FAILED";
                                    Map<String, Integer> map = new HashMap<String, Integer>();
                                    map.put("cross", R.drawable.crossmark);
                                    imgValidateResult.setImageResource(map.get("cross"));
                                    txtValidateStatusPrompt.setText("RFID FAILED");
                                    txtValidateStatusPrompt.setTextColor(Color.parseColor("#FF0000"));
                                    btnManualValidateRFID.setVisibility(View.VISIBLE);
                                    scan_failed_info.setVisibility(View.VISIBLE);
                                    edrfid.setText("");
                                }
                            });

                            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    used_ticket_info.setVisibility(View.INVISIBLE);
                                    scan_failed_info.setVisibility(View.INVISIBLE);
                                    backgroundLayer.setVisibility(View.VISIBLE);
                                    resultImageLayer.setVisibility(View.INVISIBLE);
                                    edrfid.setText("");

                                }
                            }, 3000);

                        }

                        @Override
                        public void onResponse(Call call, Response response) throws IOException {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    btnNextScan.setVisibility(View.VISIBLE);
                                    cpbLoginProgress.setIndeterminate(false);
                                    coderfid = "";
                                    edrfid.setText("");
                                }
                            });

                        }


                    });
                    // return "Not Success - code : " + response.code();




                    return true;
                }
                return false;
            }
        });




        rfidSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                SharedPreferences pref = getApplicationContext().getSharedPreferences("Profile", MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                if(isChecked){

                    editor.putBoolean("rfidUsed", true);
                    Log.i("GateAgent",""+isChecked);
                    delay = 500;
                }else{
                    editor.putBoolean("rfidUsed", false);
                    delay = 3000;
                    Log.i("GateAgent",""+isChecked);
                }
                editor.commit();

            }
        });


        btnNextScan = (Button)findViewById(R.id.btnNextScan);
        btnNextScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("GateAgent", "You clicked me");
                ValidateActivity.this.recreate();
            }
        });

        txtTutorial = (TextView)findViewById(R.id.txtTutorial);
        txtTutorial.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ValidateActivity.this, TutorialActivity.class);
                i.putExtra("sessionObj", sessionData);
                i.putExtra("token", myToken);
                startActivity(i);
                // finish();
            }
        });

        txtLogout = (TextView)findViewById(R.id.txtLogout);
        txtLogout.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent i = new Intent(ValidateActivity.this, LoginActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                finish();
            }
        });

        imgManualValidate = (ImageView)findViewById(R.id.lnkManualValidate);
        imgManualValidate.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ValidateActivity.this, ManualValidationActivity.class);
                i.putExtra("token", myToken);
                i.putExtra("sessionObj", sessionData);
                i.putExtra("RFID", ""+rfidSwitch.isChecked());
                startActivity(i);

            }
        });

        txtValidateStatusPrompt = (TextView)findViewById(R.id.validate_status_prompt);
        txtTicketBought = (TextView)findViewById(R.id.txtTicketBought);
        txtFirstName = (TextView)findViewById(R.id.txtFirstName);
        txtLastName = (TextView)findViewById(R.id.txtLastName);
        txtTicketType = (TextView)findViewById(R.id.txtTicketType);
        txtScannedDate = (TextView)findViewById(R.id.txtScannedDate);
        txtScannedTime = (TextView)findViewById(R.id.txtScannedTime);
        txtOrderId = (TextView)findViewById(R.id.txtOrderId);
        txtFailedOrderId = (TextView)findViewById(R.id.txtFailedOrderId);
        txtMySeat = (TextView)findViewById(R.id.txtMySeat);
        txtQRCode = (TextView)findViewById(R.id.txtQRCode);
        imgValidateResult = (ImageView)findViewById(R.id.imgValidateResult);

        imgValidateResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*
                validate_ticket_info = (LinearLayout)findViewById(R.id.validate_ticket_info);
                validate_ticket_info.setVisibility(View.INVISIBLE);
                Map<String, Integer> map = new HashMap<String, Integer>();
                map.put("camera_frame", R.drawable.camera_frame);
                ValidateActivity.this.imgValidateResult.setImageResource(map.get("camera_frame"));
                */

                if(scan_status.contentEquals("Ticket Scan Success")) {
                    Log.d("GateAgent", scan_status);
                } else if(scan_status.contentEquals("Ticket Not Found")) {
                    Log.d("GateAgent", scan_status);
                } else if(scan_status.contentEquals("Ticket Already Verified")) {
                    Log.d("GateAgent", scan_status);

                }

                //ValidateActivity.this.recreate(); // This solution causes a blink


            }
        });

        validate_ticket_info = (LinearLayout)findViewById(R.id.validate_ticket_info);
        validate_ticket_info.setVisibility(View.INVISIBLE);

        used_ticket_info = (LinearLayout)findViewById(R.id.used_ticket_info);
        used_ticket_info.setVisibility(View.INVISIBLE);

        scan_failed_info = (LinearLayout)findViewById(R.id.scan_failed_info);
        scan_failed_info.setVisibility(View.INVISIBLE);

        backgroundLayer = (LinearLayout)findViewById(R.id.background);
        backgroundLayer.setVisibility(View.INVISIBLE);

        validBackgroundLayer = (LinearLayout)findViewById(R.id.backgroundValid);
        validBackgroundLayer.setVisibility(View.INVISIBLE);

        resultImageLayer = (LinearLayout)findViewById(R.id.resultImageLayer);

        btnTryAgain = (Button)findViewById(R.id.btnTryAgain);
        btnTryAgain.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                ValidateActivity.this.recreate();
            }
        });

        btnManualValidate = (Button)findViewById(R.id.btnManualValidate);
        btnManualValidate.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ValidateActivity.this, ManualValidationActivity.class);
                i.putExtra("token", myToken);
                i.putExtra("sessionObj", sessionData);
                i.putExtra("RFID", "false");
                startActivity(i);

            }
        });


        imageButton = (ImageButton)findViewById(R.id.imageButton);

        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        btnHome = (ImageButton)findViewById(R.id.btnHome);

        btnHome.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Log.d("Test", "You clicked home");
                Intent i = new Intent(ValidateActivity.this, MyEventActivity.class);
                i.putExtra("sessionObj", sessionData);
                i.putExtra("token", myToken);
                startActivity(i);
            }
        });

        btnResult = (ImageButton)findViewById(R.id.btnResult);
        btnResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Test", "You clicked result");
                Intent i = new Intent(ValidateActivity.this, ResultActivity.class);
                i.putExtra("sessionObj", sessionData);
                i.putExtra("token", myToken);
                startActivity(i);
            }
        });

        boolean autoFocus = true;
        boolean useFlash = true;

        cameraView = (CameraSourcePreview) findViewById(R.id.camera_view);
        ParentView = (View) findViewById(R.id.ParentView);
        ParentView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                int actionMasked = event.getActionMasked();
                switch (actionMasked) {

                    case MotionEvent.ACTION_DOWN:
                        // show preview
                        checkTap = true;

                        break;

                    case MotionEvent.ACTION_UP:
                        checkTap = false;
                        Log.i("ValidateLOG","ACTION_UP" + actionMasked);
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        // hide preview
                        checkTap = false;
                        break;
                    case MotionEvent.ACTION_MOVE:
                        checkTap = true;
                        break;
                    case MotionEvent.ACTION_HOVER_MOVE:
                        checkTap = true;
                        break;

                    default:
                        checkTap = false;
                        break;
                }

                return true;
            }
        });


        int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if(rc == PackageManager.PERMISSION_GRANTED) {
            createCameraSource(autoFocus, useFlash);


        } else {
            requestCameraPermission();
        }




    }
    @Override
    protected void onResume() {
        super.onResume();

        startCameraSource();
    }
    @Override
    public void onPause() {
        super.onPause();

        if (cameraView != null) {
            cameraView.stop();
        }
        overridePendingTransition(0,0);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (cameraView != null) {
            cameraView.release();
        }
    }
    @Override
    public void recreate() {
        super.recreate();
        overridePendingTransition(0, 0);
    }

    private void requestCameraPermission() {


        Log.w(TAG, "Camera permission is not granted. Requesting permission");

        final String[] permission = new String[]{Manifest.permission.CAMERA};

        final Activity thisActivity = this;

        View.OnClickListener listener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(thisActivity, permission, RC_HANDLE_CAMERA_PERM);
            }
        };

    }

    private void validateCode(String ticketCode) {
        code = ticketCode;
        final String urlParameter = "token="+myToken+"&code="+code;
        Log.i("GateAgent",urlParameter);
        new ValidateTask(cpbLoginProgress).execute(urlParameter);
    }

    private class ValidateTask extends AsyncTask<String, Void, String> {
        CircularProgressBar cpbLoginProgress;

        public ValidateTask(CircularProgressBar cpbLoginProgress) {
            this.cpbLoginProgress = cpbLoginProgress;

        }

        protected void onPreExecute() {
            Log.i("GateAgent","onPreExecute"); cpbLoginProgress.setIndeterminate(true);
        }

        @Override
        protected String doInBackground(String... params) {
            int responseCode = 0;
            StringBuilder responseOutput = new StringBuilder("");

            SimpleDateFormat myDateFormat = new SimpleDateFormat("MMM dd, yyyy");
            SimpleDateFormat myTimeFormat = new SimpleDateFormat("HH:mm");
            Date now = new Date();
            scannedDate = myDateFormat.format(now);
            scannedTime = myTimeFormat.format(now);

            try {
                URL url = null;
                final Boolean[] rfidcheckkk = {true};
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        rfidcheckkk[0] = rfidSwitch.isChecked();
                        Log.i("ddddd",rfidcheckkk[0]+" fdgdfg");
                    }
                });
                Log.i("ddddd",rfidcheckkk[0]+" fdgdfg");
                if(rfidcheckkk[0]) {
                    url = new URL("https://www.ticketmelon.com/api/codes/verifyrfid");
                }else{
                    url = new URL("https://www.ticketmelon.com/api/codes/verify");
                }


                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                String urlParameters = params[0];
                connection.setRequestMethod("POST");
                connection.setRequestProperty("USER-AGENT", "Mozilla/5.0");
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestProperty("Accept", "application/json");
                connection.setDoOutput(true);
                DataOutputStream dStream = new DataOutputStream(connection.getOutputStream());
                dStream.writeBytes(urlParameters);
                dStream.flush();
                dStream.close();
                responseCode = connection.getResponseCode();
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line = "";
                responseOutput = new StringBuilder();
                while((line = br.readLine()) != null){
                    responseOutput.append(line);
                }
                br.close();

            } catch(MalformedURLException e) {

            }catch (Exception e) {

            }
            return responseOutput.toString();
        }



        protected void onPostExecute(String result) {
            cpbLoginProgress.setIndeterminate(false);

            Log.d("GateAgent", result.toString());
            try {
                JSONObject object = new JSONObject(result);

                msg = object.getString("msg");

                if(msg.contains("Ticket Not Found")) {
                    scan_status = "Ticket Not Found";
                    Map<String, Integer> map = new HashMap<String, Integer>();
                    map.put("cross", R.drawable.crossmark);
                    imgValidateResult.setImageResource(map.get("cross"));
                    txtValidateStatusPrompt.setText("VALIDATION FAILED");
                    txtValidateStatusPrompt.setTextColor(Color.parseColor("#FF0000"));
                    scan_failed_info.setVisibility(View.VISIBLE);


                    // String code, String type, String time, String status, String date_scanned, String orderNumber, String agentUser, String eventName
                    Ticket ticket = new Ticket(code, "NA", scannedTime, "Failed", scannedDate, "NA", sessionData.getUsername(), sessionData.getEvent_name());
                    DatabaseHandler db = new DatabaseHandler(ValidateActivity.this.getApplicationContext());
                    db.addTicket(ticket);
                    db.close();

                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            used_ticket_info.setVisibility(View.INVISIBLE);
                            scan_failed_info.setVisibility(View.VISIBLE);
                            backgroundLayer.setVisibility(View.VISIBLE);
                            resultImageLayer.setVisibility(View.INVISIBLE);


                        }
                    }, 3000);
                    Log.d("GateAgent", msg);

                }else if(msg.contains("is already verified")) {
                    purchased_datetime = object.getString("purchased_at");
                    String scanned_datetime = object.getString("verify_at");
                    order_id = object.getString("order_id");
                    ticket_type = object.getString("ticket_type");
                    buyer = object.getString("buyer");
                    buyer_lastname = object.getString("buyer_lastname");
                    verify_at = object.getString("verify_at");
                    verified_time = verify_at.substring(0, 5);
                    verified_date = verify_at.substring(6);
                    if(!msg.isEmpty()) {
                        barcode = msg.substring(0,11);
                    }
                    Log.d("GateAgent", "Last Name: " + buyer_lastname);
                    Log.d("GateAgent", "Verified at : " + scanned_datetime);

                    txtQRCode.post(new Runnable() {
                        @Override
                        public void run() {
                            txtTicketType.setText(ticket_type.toUpperCase());
                            txtQRCode.setText("Entrant Code: " + barcode);
                            txtTicketBought.setText("Ticket Bought: " + purchased_datetime);
                            txtOrderId.setText("Order ID: " + order_id);
                            txtFirstName.setText(buyer);

                            Map<String, Integer> map = new HashMap<String, Integer>();
                            scan_status = "Ticket Already Verified";
                            // Ticket has been used
                            txtFailedOrderId.setText("Order ID: " + order_id);
                            map.put("cross", R.drawable.crossmark);
                            imgValidateResult.setImageResource(map.get("cross"));
                            txtValidateStatusPrompt.setText("VALIDATION FAILED");
                            txtValidateStatusPrompt.setTextColor(Color.parseColor("#FF0000"));
                            used_ticket_info.setVisibility(View.VISIBLE);
                            txtScannedDate.setText(verified_date);
                            txtScannedTime.setText(verified_time);

                            // String code, String type, String time, String status, String date_scanned, String orderNumber, String agentUser, String eventName
                            Ticket ticket = new Ticket(code, ticket_type, scannedTime, "Used", scannedDate, order_id, sessionData.getUsername(), sessionData.getEvent_name());
                            DatabaseHandler db = new DatabaseHandler(ValidateActivity.this.getApplicationContext());
                            db.addTicket(ticket);
                            db.close();

                            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    used_ticket_info.setVisibility(View.INVISIBLE);
                                    scan_failed_info.setVisibility(View.VISIBLE);
                                    backgroundLayer.setVisibility(View.VISIBLE);
                                    resultImageLayer.setVisibility(View.INVISIBLE);


                                }
                            }, 3000);
                        }
                    });

                }else if(msg.isEmpty()) {

                    // Successful scan : valid ticket
                    purchased_datetime = object.getString("purchased_at");
                    String scanned_datetime = object.getString("verify_at");
                    order_id = object.getString("order_id");
                    JSONObject myObj = (JSONObject)object.getJSONArray("seat").get(0);
                    seatNumber = myObj.getString("number");
                    seatRow = myObj.getString("row");
                    seatSection = myObj.getString("section");
                    mySeat = seatSection + "  /  " + seatRow + "  /  " + seatNumber;
                    Log.d("GateAgent", "Seat: " + mySeat);
                    ticket_type = object.getString("ticket_type");
                    buyer = object.getString("buyer");
                    user_id = object.getString("user_id");
                    Log.d("GateAgent", "buyer: " + buyer);
                    Log.d("GateAgent", "userid: " + user_id);
                    buyer_lastname = object.getString("buyer_lastname");
                    verify_at = object.getString("verify_at");
                    verified_time = verify_at.substring(0, 5);
                    verified_date = verify_at.substring(6);
                    if(!msg.isEmpty()) {
                        barcode = msg.substring(0,11);
                    }
                    Log.d("GateAgent", "Last Name: " + buyer_lastname);
                    Log.d("GateAgent", "Verified at : " + scanned_datetime);
                    scan_status = "Ticket Scan Success";



                    txtQRCode.post(new Runnable() {
                        @Override
                        public void run() {
                            if(rfidSwitch.isChecked()){
                                // switchStatus.setText("Switch is currently ON");
                                btnNextScan.setVisibility(View.GONE);
                                edrfid.setVisibility(View.VISIBLE);
                                coderfid = "";
                                edrfid.setText("");
                                edrfid.requestFocus();

                            }
                            Map<String, Integer> map = new HashMap<String, Integer>();
                            map.put("checkmark", R.drawable.checkmark);
                            txtQRCode.setText("Entrant Code: " + code);
                            txtMySeat.setText(mySeat);
                            txtFirstName.setText(buyer);
                            txtLastName.setText(buyer_lastname);
                            txtTicketType.setText(ticket_type);
                            txtTicketBought.setText("Ticket Bought: " + purchased_datetime);
                            txtFailedOrderId.setText("Order ID: " + order_id);
                            imgValidateResult.setImageResource(map.get("checkmark"));
                            txtValidateStatusPrompt.setText("VALIDATION SUCCESS");
                            txtValidateStatusPrompt.setTextColor(Color.parseColor("#48C8F3"));
                            validate_ticket_info.setVisibility(View.VISIBLE);
                        }
                    });



                    // String code, String type, String time, String status, String date_scanned, String orderNumber, String agentUser, String eventName
                    Ticket ticket = new Ticket(code, ticket_type, scannedTime, "Success", scannedDate, order_id, sessionData.getUsername(), sessionData.getEvent_name());
                    DatabaseHandler db = new DatabaseHandler(ValidateActivity.this.getApplicationContext());
                    db.addTicket(ticket);
                    db.close();

                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("GateAgent", "Displaying Valid Ticket Background");

                            // used_ticket_info.setVisibility(View.INVISIBLE);
                            // scan_failed_info.setVisibility(View.VISIBLE);
                            validBackgroundLayer.setVisibility(View.VISIBLE);
                            resultImageLayer.setVisibility(View.INVISIBLE);


                        }
                    }, delay);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private void createCameraSource(boolean autoFocus, boolean useFlash) {
        Context context = getApplicationContext();
        barcodeDetector =  new BarcodeDetector.Builder(context).build();
        //cameraSource = new CameraSource.Builder(this, barcodeDetector).setRequestedPreviewSize(640,480).setAutoFocusEnabled(true).build();
        if (!barcodeDetector.isOperational()) {
            // Note: The first time that an app using the barcode or face API is installed on a
            // device, GMS will download a native libraries to the device in order to do detection.
            // Usually this completes before the app is run for the first time.  But if that
            // download has not yet completed, then the above call will not detect any barcodes
            // and/or faces.
            //
            // isOperational() can be used to check if the required native libraries are currently
            // available.  The detectors will automatically become operational once the library
            // downloads complete on device.
            Log.w(TAG, "Detector dependencies are not yet available.");

            // Check for low storage.  If there is low storage, the native library will not be
            // downloaded, so detection will not become operational.
            IntentFilter lowstorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
            boolean hasLowStorage = registerReceiver(null, lowstorageFilter) != null;


        }

        CameraSource.Builder builder = new CameraSource.Builder(getApplicationContext(), barcodeDetector)
                .setRequestedPreviewSize(1600, 1024)
                .setRequestedFps(15.0f);

        // make sure that auto focus is an available option
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            builder = builder.setFocusMode(
                    autoFocus ? Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE : null);
        }

        cameraSource = builder
                .setFlashMode(Camera.Parameters.FLASH_MODE_OFF)
                .build();

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {

            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();
                if (barcodes.size() != 0) {
                    barcodeDetector.release();

                    String tmp = barcodes.valueAt(0).displayValue;
                    final String barcode = tmp.substring(0, 3) + "-" + tmp.substring(4, 7) + "-" + tmp.substring(8, 11);
                    Log.d("GateAgent", "Code detected " + barcodes.valueAt(0).displayValue);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            validateCode(barcode);
                            ParentView.setVisibility(View.GONE);
                        }
                    });


                    /*
                    txtQRCode.post(new Runnable() {
                        @Override
                        public void run() {
                            txtQRCode.setText("Entrant Code: " + barcodes.valueAt(0).displayValue);
                            Map<String, Integer> map = new HashMap<String, Integer>();
                            map.put("checkmark", R.drawable.checkmark);
                            imgValidateResult.setImageResource(map.get("checkmark"));
                            validate_ticket_info.setVisibility(View.VISIBLE);
                            txtValidateStatusPrompt.setText("VALIDATION SUCCESS");
                        }
                    });
                    */


                }
            }
        });



    }
    private void startCameraSource() throws SecurityException {
        // check that the device has play services available.
        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
                getApplicationContext());
        if (code != ConnectionResult.SUCCESS) {
            Dialog dlg =
                    GoogleApiAvailability.getInstance().getErrorDialog(this, code, 9001);
            dlg.show();
        }

        if (cameraSource != null) {
            try {
                cameraView.start(cameraSource);
            } catch (IOException e) {
                Log.e(TAG, "Unable to start camera source.", e);
                cameraSource.release();
                cameraSource = null;
            }
        }
    }
}
