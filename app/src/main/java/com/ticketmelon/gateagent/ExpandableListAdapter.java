package com.ticketmelon.gateagent;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by cube on 2/18/2016.
 */
public class ExpandableListAdapter extends BaseExpandableListAdapter implements Filterable {

    private Context _context;
    private List<Ticket> _listDataHeader; // Header title

    private List<Ticket> _originalList;

    private Map<String, List<Ticket>> _ticketCollections;



    // Map<String,List<Ticket>> listDataChild, List<Ticket> ticketCollection
    public ExpandableListAdapter(Context context, List<Ticket> listDataHeader, Map<String, List<Ticket>> ticketCollections) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._originalList = this._listDataHeader;
        this._ticketCollections = ticketCollections;
    }


    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._ticketCollections.get(_listDataHeader.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        Log.d("GateAgent", "Balls");
        return this._ticketCollections.get(_listDataHeader.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        Ticket headerObject = (Ticket)getGroup(groupPosition);

        if(convertView == null) {
           // LayoutInflater infalInflater = (LayoutInflater)this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            LayoutInflater inflater = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.result_group, null);
        }
        TextView txtCode = (TextView)convertView.findViewById(R.id.txtResultCode);
        txtCode.setText(headerObject.getCode());

        TextView txtType = (TextView)convertView.findViewById(R.id.txtResultType);
        txtType.setText(headerObject.getType());

        TextView txtTime = (TextView)convertView.findViewById(R.id.txtResultTime);
        txtTime.setText(headerObject.getTime());

        TextView txtStatus = (TextView)convertView.findViewById(R.id.txtResultStatus);
        txtStatus.setText(headerObject.getStatus());

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        Log.d("GateAgent", "Fish");
        Ticket headerObject = (Ticket)getChild(groupPosition, childPosition);
        if(convertView == null) {
            //LayoutInflater ifalInflater = (LayoutInflater)this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            LayoutInflater inflater = LayoutInflater.from(_context);
            convertView = inflater.inflate(R.layout.result_item, null);
            TextView txtOrderNumber = (TextView)convertView.findViewById(R.id.txtExpandedOrderNumber);
            txtOrderNumber.setText(headerObject.getOrderNumber());

            TextView txtTime = (TextView)convertView.findViewById(R.id.txtExpandedResultTime);
            txtTime.setText(headerObject.getTime());

            TextView txtDate = (TextView)convertView.findViewById(R.id.txtExpandedResultDate);
            txtDate.setText(headerObject.getDate_scanned());
        }
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new MyFilter();
        return filter;
    }

    private class MyFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            List<Ticket> FilteredListDataHeader = new ArrayList<Ticket>();

            constraint = constraint.toString().toLowerCase();
            for(int i = 0; i < _listDataHeader.size(); i++) {
                Ticket ticket = _listDataHeader.get(i);
                Log.d("GateAgent", constraint.toString());
                String code = ticket.getCode().toLowerCase();
                Log.d("GateAgent", code);
                if(code.startsWith(constraint.toString())){
                    FilteredListDataHeader.add(ticket);
                    Log.d("GateAgent", code.charAt(0) + " = " + constraint.charAt(0));
                }
                if(constraint.toString().isEmpty()) {
                    FilteredListDataHeader = _listDataHeader;
                }
            }
            results.count = FilteredListDataHeader.size();
            results.values = FilteredListDataHeader;
            Log.d("GateAgent", results.values.toString());
            Log.d("GateAgent", String.valueOf(_listDataHeader.size()));

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if(!results.values.toString().isEmpty()) {
                _listDataHeader = (List<Ticket>)results.values;
            } else {
                _listDataHeader = _originalList;
            }

            notifyDataSetChanged();
        }
    }
}
