package com.ticketmelon.gateagent;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.net.HttpURLConnection;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
public class MyEventActivity extends AppCompatActivity {

    Toolbar toolbar;

    ImageButton btnHome, btnValidate, btnResult;

    ImageButton imageButton;

    DrawerLayout mDrawerLayout;

    String poster;
    String event_name;
    String myToken;

    ImageView imgPoster;
    TextView txtEventName,txtEventDate, txtTutorial, txtLogout;

    String[] monthArray = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    String date, day, month, year;

    Session sessionData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_event);

        Intent i = getIntent();
        if(i != null && i.getExtras() != null) {
            sessionData = (Session)i.getSerializableExtra("sessionObj");
            // String username = i.getExtras().getString("username");
            String username = sessionData.getUsername();
            // event_name = i.getExtras().getString("event_name");
            event_name = sessionData.getEvent_name();
            // poster = i.getExtras().getString("poster");
            poster = sessionData.getPoster();
            // String[] dayTime = i.getExtras().getString("date").split(" ");
            String[] dayTime = sessionData.getDate().split(" ");
            date = dayTime[0];
            String[] tmp = date.split("-");
            year = tmp[0];
            int month_index = Integer.parseInt(tmp[1]);
            month = monthArray[month_index];
            day = tmp[2];
            //myToken = i.getExtras().getString("token");
            myToken = sessionData.getToken();
        }

        txtEventName = (TextView)findViewById(R.id.txtEventName);
        txtEventName.setText(event_name);

        txtEventDate = (TextView)findViewById(R.id.txtEventDate);
        txtEventDate.setText(month + " " + day + ", " + year);

        imgPoster = (ImageView)findViewById(R.id.imgPoster);



        String posterURL = "http://" + poster;
        Log.d("GateAgent",posterURL);

        new DownloadPosterTask((ImageView)findViewById(R.id.imgPoster)).execute(posterURL);



        imageButton = (ImageButton)findViewById(R.id.imageButton);

        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
        });


        btnValidate = (ImageButton)findViewById(R.id.btnValidate);
        btnValidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(MyEventActivity.this, ValidateActivity.class);
                i.putExtra("token", myToken);
                i.putExtra("sessionObj", sessionData);
                startActivity(i);
            }
        });

        btnResult = (ImageButton)findViewById(R.id.btnResult);
        btnResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(MyEventActivity.this, ResultActivity.class);
                i.putExtra("sessionObj", sessionData);
                i.putExtra("token", myToken);
                startActivity(i);

            }
        });

        txtTutorial = (TextView)findViewById(R.id.txtTutorial);
        txtTutorial.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MyEventActivity.this, TutorialActivity.class);
                i.putExtra("sessionObj", sessionData);
                i.putExtra("token", myToken);
                startActivity(i);
                // finish();
            }
        });

        txtLogout = (TextView)findViewById(R.id.txtLogout);
        txtLogout.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent i = new Intent(MyEventActivity.this, LoginActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                finish();
            }
        });





    }

    private class DownloadPosterTask extends AsyncTask<String, Void, Bitmap> {

        ImageView imgPoster;

        public DownloadPosterTask(ImageView imgPoster) {

            this.imgPoster = imgPoster;

        }

        @Override
        protected Bitmap doInBackground(String... params) {

            String urldisplay = params[0];
            Bitmap mIcon11 = null;


            try {
                InputStream in = new java.net.URL(urldisplay).openStream();

                mIcon11 = BitmapFactory.decodeStream(in);

            } catch (Exception e) {
                Log.d("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            // Log.v("GateAgent", result.toString());
            // imgPoster.setImageBitmap(result);
            MyEventActivity.this.updatePoster(result);

        }
    }

    public void updatePoster(Bitmap myImage) {
        imgPoster.setImageBitmap(myImage);
    }






}
