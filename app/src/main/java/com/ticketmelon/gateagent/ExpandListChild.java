package com.ticketmelon.gateagent;

/**
 * Created by cube on 3/25/2016.
 */
public class ExpandListChild {
    private String order_id;
    private String scanned_time;
    private String scanned_date;

    public ExpandListChild(String order_id, String scanned_time, String scanned_date) {
        this.order_id = order_id;
        this.scanned_time = scanned_time;
        this.scanned_date = scanned_date;
    }

    public ExpandListChild() {

    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getScanned_time() {
        return scanned_time;
    }

    public void setScanned_time(String scanned_time) {
        this.scanned_time = scanned_time;
    }

    public String getScanned_date() {
        return scanned_date;
    }

    public void setScanned_date(String scanned_date) {
        this.scanned_date = scanned_date;
    }
}
