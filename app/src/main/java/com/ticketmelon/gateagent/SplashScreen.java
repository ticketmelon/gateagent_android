package com.ticketmelon.gateagent;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

public class SplashScreen extends AppCompatActivity {
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 6000;

    final String PREFS_NAME = "MyPrefsFile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);




        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);

                if(settings.getBoolean("my_first_time", true)) {
                    // The app is being launched for the first time, do something
                    Log.d("Comments", "First time");

                    // First time task
                    // Run tutorial
                    Intent i = new Intent(SplashScreen.this, TutorialActivity.class);
                    startActivity(i);

                    // Record the fact that the app has been started at  least once
                    settings.edit().putBoolean("my_first_time", false).commit();
                } else {
                    // This method will be executed once the timer is over
                    Intent i = new Intent(SplashScreen.this, LoginActivity.class);
                    startActivity(i);
                }



                // Close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
