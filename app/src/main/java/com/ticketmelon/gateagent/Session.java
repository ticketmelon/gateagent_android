package com.ticketmelon.gateagent;

import java.io.Serializable;

/**
 * Created by cube on 3/23/2016.
 */
public class Session implements Serializable {
    private String token;
    private String poster;
    private String event_name;
    private String username;
    private String ticket_id;
    private String date;

    public Session(String token, String poster, String event_name, String username, String ticket_id, String date) {
        this.token = token;
        this.poster = poster;
        this.event_name = event_name;
        this.username = username;
        this.ticket_id = ticket_id;
        this.date = date;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTicket_id() {
        return ticket_id;
    }

    public void setTicket_id(String ticket_id) {
        this.ticket_id = ticket_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
