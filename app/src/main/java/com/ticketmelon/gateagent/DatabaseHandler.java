package com.ticketmelon.gateagent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cube on 3/7/2016.
 */
public class DatabaseHandler extends SQLiteOpenHelper {
    // All static variables
    // Database version
    private static final int DATABASE_VERSION = 1;

    // Database name
    private static final String DATABASE_NAME = "gaDatabase";

    // Tickets table name
    private static final String TABLE_TICKETS = "tickets";

    // Tickets table column names
    private static final String KEY_ID = "id";
    private static final String KEY_CODE = "code";
    private static final String KEY_TYPE = "type";
    private static final String KEY_STATUS = "status";
    private static final String KEY_ORDERNUMBER = "ordernumber";
    private static final String KEY_TIMESCANNED = "timescanned";
    private static final String KEY_DATESCANNED = "datescanned";
    private static final String KEY_AGENTUSER = "agentuser";
    private static final String KEY_EVENTNAME = "eventname";


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TICKETS_TABLE = "CREATE TABLE " + TABLE_TICKETS + "(" + KEY_ID + " INTEGER PRIMARY KEY, " + KEY_CODE + " TEXT, " + KEY_TYPE + " TEXT, " + KEY_STATUS + " TEXT, " + KEY_ORDERNUMBER + " TEXT, " + KEY_TIMESCANNED + " TEXT, " + KEY_DATESCANNED + " TEXT, " + KEY_AGENTUSER + " TEXT, " + KEY_EVENTNAME + " TEXT)";
        db.execSQL(CREATE_TICKETS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TICKETS);

        // Create tables again
        onCreate(db);
    }

    // Adding new ticket
    public void addTicket(Ticket ticket) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_CODE, ticket.getCode());
        values.put(KEY_TYPE, ticket.getType());
        values.put(KEY_STATUS, ticket.getStatus());
        values.put(KEY_ORDERNUMBER, ticket.getOrderNumber());
        values.put(KEY_TIMESCANNED, ticket.getTime_scanned());
        values.put(KEY_DATESCANNED, ticket.getDate_scanned());
        values.put(KEY_AGENTUSER, ticket.getAgentUser());
        values.put(KEY_EVENTNAME, ticket.getEventName());

        // Inserting Row
        db.insert(TABLE_TICKETS, null, values);
        db.close();
    }

    // Retrieving all tickets
    public List<Ticket> getAllTickets(String username) {
        List<Ticket> ticketList = new ArrayList<Ticket>();

        // Select all query
        String selectQuery = "SELECT * FROM " + TABLE_TICKETS + " WHERE agentuser = '" + username + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        Log.v("GateAgent", "Data Count : " + String.valueOf(cursor.getCount()));

        // Looping through all rows and adding to list
        if(cursor.moveToFirst()) {
            do {
                Ticket ticket = new Ticket();
                ticket.setId(Integer.parseInt(cursor.getString(0)));
                ticket.setCode(cursor.getString(1));
                ticket.setType(cursor.getString(2));
                ticket.setStatus(cursor.getString(3));
                ticket.setOrderNumber(cursor.getString(4));
                ticket.setTime_scanned(cursor.getString(5));
                ticket.setDate_scanned(cursor.getString(6));
                ticket.setAgentUser(cursor.getString(7));
                ticket.setEventName(cursor.getString(8));
                ticketList.add(ticket);
            } while(cursor.moveToNext());
        }
        return ticketList;
    }
}
