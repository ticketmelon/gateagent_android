package com.ticketmelon.gateagent;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by cube on 3/25/2016.
 */
public class ExpandListAdapter extends BaseExpandableListAdapter implements Filterable{

    private Context context;
    private ArrayList<ExpandListGroup> groups;
    private ArrayList<ExpandListGroup> original_groups;

    public ExpandListAdapter(Context context, ArrayList<ExpandListGroup> groups) {
        this.context = context;
        this.groups = groups;
        this.original_groups = groups;
    }
    public void addSet(ArrayList<ExpandListGroup> groups){
        this.groups = groups;
        this.original_groups = groups;
    }
    public void addItem(ExpandListChild item, ExpandListGroup group) {
        if(!groups.contains(group)) {
            groups.add(group);
        }
        int index = groups.indexOf(group);
        ExpandListChild ch = item;
        groups.get(index).setItems(ch);
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        ExpandListChild ch = groups.get(groupPosition).getItems();
        return ch;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        ExpandListGroup group = (ExpandListGroup)getGroup(groupPosition);
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.result_group, null);

        }
        TextView txtCode = (TextView)convertView.findViewById(R.id.txtResultCode);
        txtCode.setText(group.getCode());

        TextView txtType = (TextView)convertView.findViewById(R.id.txtResultType);
        txtType.setText(group.getType());

        TextView txtTime = (TextView)convertView.findViewById(R.id.txtResultTime);
        txtTime.setText(group.getScanned_time());

        TextView txtStatus = (TextView)convertView.findViewById(R.id.txtResultStatus);
        txtStatus.setText(group.getIssue());

        ImageView imgResultDropdown = (ImageView)convertView.findViewById(R.id.imgResultDropdown);

        if(isExpanded) {
            Map<String, Integer> map = new HashMap<String, Integer>();
            map.put("expanded", R.drawable.result_arrow_dropdown_clicked);
            // imgValidateResult.setImageResource(map.get("checkmark"));
            imgResultDropdown.setImageResource(map.get("expanded"));

            // groupBackground.setBackgroundResource(R.drawable.expanded_border);
        } else {
            Map<String, Integer> map = new HashMap<String, Integer>();
            map.put("collapsed", R.drawable.result_arrow_dropdown);
            // imgValidateResult.setImageResource(map.get("checkmark"));
            imgResultDropdown.setImageResource(map.get("collapsed"));
            // groupBackground.setBackgroundResource(R.drawable.collapsed_border);
        }
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ExpandListChild ch = (ExpandListChild)getChild(groupPosition, childPosition);
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.result_item, null);

        }

        TextView txtOrderNumber = (TextView)convertView.findViewById(R.id.txtExpandedOrderNumber);
        txtOrderNumber.setText(ch.getOrder_id());

        TextView txtTime = (TextView)convertView.findViewById(R.id.txtExpandedResultTime);
        txtTime.setText(ch.getScanned_time());

        TextView txtDate = (TextView)convertView.findViewById(R.id.txtExpandedResultDate);
        txtDate.setText(ch.getScanned_date());
        return convertView;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new MyFilter();
        return filter;
    }

    private class MyFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            groups = original_groups;
            List<ExpandListGroup> filteredListGroup = new ArrayList<ExpandListGroup>();
            if (constraint.length() != 0) {
                constraint = constraint.toString().toLowerCase();
                for(int i = 0; i < groups.size(); i++) {
                    ExpandListGroup group = groups.get(i);
                    Log.v("GateAgent", "Filter by: " + constraint.toString());
                    String code = group.getCode().toLowerCase();
                    Log.v("GateAgent", "Valid code: " + code);
                    if(code.startsWith(constraint.toString())){
                        filteredListGroup.add(group);
                        Log.v("GateAgent", code.charAt(0) + " = " + constraint.charAt(0));
                    }

                }
            } else {
                groups = original_groups;
                filteredListGroup = groups;
            }




            results.count = filteredListGroup.size();
            results.values = filteredListGroup;
            Log.v("GateAgent", results.values.toString());
            Log.v("GateAgent", String.valueOf(groups.size()));

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            groups = (ArrayList<ExpandListGroup>) results.values;
            notifyDataSetChanged();

        }
    }



    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
