package com.ticketmelon.gateagent;

import java.util.ArrayList;

/**
 * Created by cube on 3/25/2016.
 */
public class ExpandListGroup {
    private String code;
    private String type;
    private String scanned_time;
    private String issue;
    private ExpandListChild items;

    public ExpandListGroup(String code, String type, String scanned_time, String issue, ExpandListChild items) {
        this.code = code;
        this.type = type;
        this.scanned_time = scanned_time;
        this.issue = issue;
        this.items = items;
    }

    public ExpandListGroup() {

    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getScanned_time() {
        return scanned_time;
    }

    public void setScanned_time(String scanned_time) {
        this.scanned_time = scanned_time;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public ExpandListChild getItems() {
        return items;
    }

    public void setItems(ExpandListChild items) {
        this.items = items;
    }
}
