package com.ticketmelon.gateagent;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import fr.castorflex.android.circularprogressbar.CircularProgressBar;
import fr.castorflex.android.circularprogressbar.CircularProgressDrawable;


public class LoginActivity extends AppCompatActivity {

    Button btnLogin;

    TextView txtUsername, txtPassword;

    String username, password;

    CircularProgressBar cpbLoginProgress;

    Button btnDialogDismiss;

    Session sessionData;
    int MY_PERMISSIONS_REQUEST_Camera = 101;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txtUsername = (TextView)findViewById(R.id.txtLoginUsername);
        txtPassword = (TextView)findViewById(R.id.txtLoginPassword);
        txtUsername.setTypeface(Typeface.DEFAULT );
        txtUsername.setHint(Html.fromHtml("<font color='#767C80'>Username</font>"));
        txtPassword.setTypeface(Typeface.DEFAULT);
        txtPassword.setHint(Html.fromHtml("<font color='#767C80'>Password</font>"));

        cpbLoginProgress = (CircularProgressBar)findViewById(R.id.cpbLoginProgress);
        cpbLoginProgress.setIndeterminate(false);

        btnDialogDismiss = (Button)findViewById(R.id.btnOK);






        btnLogin = (Button)findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v("GateAgent", "Attempt Login");



                username = txtUsername.getText().toString();
                password = txtPassword.getText().toString();

                final String urlParameter = "username="+username+"&password="+password;
                Log.v("GateAgent", "URL Parameter : " + urlParameter);


                new LoginTask(cpbLoginProgress).execute(urlParameter);

            }
        });

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CAMERA)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST_Camera);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    private class LoginTask extends AsyncTask<String, Void, String> {
        CircularProgressBar cpbLoginProgress;

        public LoginTask(CircularProgressBar cpbLoginProgress) {
            this.cpbLoginProgress = cpbLoginProgress;

        }

        @Override
        protected String doInBackground(String... params) {
            int responseCode = 0;
            StringBuilder responseOutput = new StringBuilder("");

            Log.v("GateAgent", "Log in with " + params[0]);




            try {

                URL url = new URL("https://www.ticketmelon.com/galogin");
                HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                String urlParameters = params[0];
                connection.setRequestMethod("POST");
                connection.setRequestProperty("USER-AGENT", "Mozilla/5.0");
                connection.setDoOutput(true);
                DataOutputStream dStream = new DataOutputStream(connection.getOutputStream());
                dStream.writeBytes(urlParameters);
                dStream.flush();
                dStream.close();
                responseCode = connection.getResponseCode();
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line = "";
                responseOutput = new StringBuilder();
                while((line = br.readLine()) != null){
                    responseOutput.append(line);
                }
                br.close();
            } catch (MalformedURLException e) {

            } catch (Exception e) {
                Log.v("GateAgent", e.getMessage().toString());
            }

            return responseOutput.toString();
        }

        protected void onPreExecute() {
            cpbLoginProgress.setIndeterminate(true);
        }

        protected void onPostExecute(String result) {
            cpbLoginProgress.setIndeterminate(false);
            Log.d("GateAgent", result);
            try {
                JSONObject object = new JSONObject(result);
                JSONObject dataObject = object.getJSONObject("user");
                JSONObject dateObject = dataObject.getJSONObject("created_at");

                String myToken = dataObject.getString("token");
                String myPoster = dataObject.getString("poster");
                String myEventName = dataObject.getString("event_name");
                String myUsername = dataObject.getString("username");
                String myTicketID = dataObject.getString("ticket_id");
                String myCreatedAt = dateObject.getString("date");

                // String token, String poster, String event_name, String username, String ticket_id, String date
                sessionData = new Session(myToken, myPoster, myEventName, myUsername, myTicketID, myCreatedAt);


                Intent i = new Intent(LoginActivity.this, MyEventActivity.class);
                i.putExtra("token", myToken);
                i.putExtra("poster", myPoster);
                i.putExtra("event_name", myEventName);
                i.putExtra("username", myUsername);
                i.putExtra("ticket_id", myTicketID);
                i.putExtra("date", myCreatedAt);
                i.putExtra("sessionObj", sessionData);
                startActivity(i);
            } catch (Exception e) {
                Log.v("GateAgent", "Login Failed 2");
                final Dialog dialog = new Dialog(LoginActivity.this);

                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.sign_in_dialog);
                dialog.show();

                btnDialogDismiss = (Button)dialog.findViewById(R.id.btnOK);
                btnDialogDismiss.setOnClickListener(new View.OnClickListener(){

                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });


            }

            Log.v("GateAgent", result);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 101 : {
                Log.i("aaa","aaa");
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}
