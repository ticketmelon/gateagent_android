package com.ticketmelon.gateagent;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import java.net.HttpURLConnection;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import fr.castorflex.android.circularprogressbar.CircularProgressBar;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ManualValidationActivity extends AppCompatActivity {

    Session sessionData;

    CircularProgressBar cpbLoginProgress;

    EditText txtrfid ,txtCodeBox1, txtCodeBox2, txtCodeBox3, txtCodeBox4, txtCodeBox5, txtCodeBox6, txtCodeBox7, txtCodeBox8, txtCodeBox9;
    Button btnSearch;

    ImageButton btnHome, btnResult;

    ImageButton imageButton;
    DrawerLayout mDrawerLayout;

    ImageView imageManualValidateResult, imgScanValidation;


String detailrfid = "";
    LinearLayout usedTicketInfo, resultManualImageLayer, mvbackground, manual_scan_failed_info, validate_manual_ticket_info, backgroundManualValid;

    TextView txt_validate_status_prompt, txtManualScannedDate, txtManualScannedTime, txtManualUsedOrderId, txtManualTicketType, txtManualFirstName, txtManualLastName, txtManualQRCode, txtManualTicketBought, txtManualOrderId, txtMySeatManual;

    String RFIDCheck,myToken, msg, verify_at, verified_time, verified_date, order_id, scan_status, code, scannedDate, scannedTime, ticket_type, fName, lName, purchased_datetime, seatNumber, seatRow, seatSection, mySeat;

    Button btnManualTryAgain, btnManualNextScan, btnCancel;

    TextView txtTutorial, txtLogout , tvrfid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual_validation);

        Intent i = getIntent();
        if(i != null && i.getExtras() != null) {
            myToken = i.getExtras().getString("token");
            sessionData = (Session)i.getSerializableExtra("sessionObj");
            RFIDCheck = i.getExtras().getString("RFID");
            Log.i("ManualValidationAC","RFID check: " + RFIDCheck + "Token: "+myToken);
        }
        txtrfid = (EditText)findViewById(R.id.txtrfid);
        tvrfid = (TextView)findViewById(R.id.tvrfid);
        tvrfid.setVisibility(View.INVISIBLE);
        txtrfid.setVisibility(View.INVISIBLE);
        if (RFIDCheck.equals("true")){
            txtrfid.setVisibility(View.VISIBLE);
            tvrfid.setVisibility(View.VISIBLE);
        }

        cpbLoginProgress = (CircularProgressBar)findViewById(R.id.cpbLoginProgress);
        cpbLoginProgress.setIndeterminate(false);

        imageManualValidateResult = (ImageView)findViewById(R.id.imgManualValidateResult);

        imgScanValidation = (ImageView)findViewById(R.id.imgAutoValidate);
        imgScanValidation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });

        btnHome = (ImageButton)findViewById(R.id.btnHome);
        btnHome.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Log.v("Test", "You clicked home");
                Intent i = new Intent(ManualValidationActivity.this, MyEventActivity.class);
                i.putExtra("sessionObj", sessionData);
                i.putExtra("token", myToken);
                startActivity(i);
            }
        });

        btnResult = (ImageButton)findViewById(R.id.btnResult);
        btnResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v("Test", "You clicked result");
                Intent i = new Intent(ManualValidationActivity.this, ResultActivity.class);
                i.putExtra("sessionObj", sessionData);
                i.putExtra("token", myToken);
                startActivity(i);
            }
        });

        txtTutorial = (TextView)findViewById(R.id.txtTutorial);
        txtTutorial.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ManualValidationActivity.this, TutorialActivity.class);
                i.putExtra("sessionObj", sessionData);
                i.putExtra("token", myToken);
                startActivity(i);

            }
        });

        txtLogout = (TextView)findViewById(R.id.txtLogout);
        txtLogout.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent i = new Intent(ManualValidationActivity.this, LoginActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                finish();
            }
        });

        validate_manual_ticket_info = (LinearLayout)findViewById(R.id.validate_manual_ticket_info);
        validate_manual_ticket_info.setVisibility(View.INVISIBLE);

        backgroundManualValid = (LinearLayout)findViewById(R.id.backgroundManualValid);
        backgroundManualValid.setVisibility(View.INVISIBLE);

        txtManualTicketType = (TextView)findViewById(R.id.txtManualTicketType);

        txtManualQRCode = (TextView)findViewById(R.id.txtManualQRCode);

        txtManualTicketBought = (TextView)findViewById(R.id.txtManualTicketBought);

        txtManualOrderId = (TextView)findViewById(R.id.txtManualOrderId);
        txtMySeatManual = (TextView)findViewById(R.id.txtMySeatManual);
        txtManualFirstName = (TextView)findViewById(R.id.txtManualFirstName);
        txtManualLastName = (TextView)findViewById(R.id.txtManualLastName);

        imageButton = (ImageButton)findViewById(R.id.imageButton);

        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        btnCancel = (Button)findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                txtCodeBox1.setText("");
                txtCodeBox2.setText("");
                txtCodeBox3.setText("");
                txtCodeBox4.setText("");
                txtCodeBox5.setText("");
                txtCodeBox6.setText("");
                txtCodeBox7.setText("");
                txtCodeBox8.setText("");
                txtCodeBox9.setText("");
                txtCodeBox1.requestFocus();
            }
        });

        btnManualTryAgain = (Button)findViewById(R.id.btnManualTryAgain);
        btnManualTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usedTicketInfo.setVisibility(View.INVISIBLE);
                manual_scan_failed_info.setVisibility(View.INVISIBLE);
                resultManualImageLayer.setVisibility(View.INVISIBLE);
                mvbackground.setVisibility(View.INVISIBLE);
                enableInput();
                txtCodeBox1.setText("");
                txtCodeBox2.setText("");
                txtCodeBox3.setText("");
                txtCodeBox4.setText("");
                txtCodeBox5.setText("");
                txtCodeBox6.setText("");
                txtCodeBox7.setText("");
                txtCodeBox8.setText("");
                txtCodeBox9.setText("");
                txtCodeBox1.requestFocus();
                txt_validate_status_prompt.setText("MANUAL VALIDATION");
                txt_validate_status_prompt.setTextColor(Color.parseColor("#FFFFFF"));
            }
        });

        btnManualNextScan = (Button)findViewById(R.id.btnManualNextScan);
        btnManualNextScan.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                // usedTicketInfo.setVisibility(View.INVISIBLE);
                // resultManualImageLayer.setVisibility(View.INVISIBLE);
                // mvbackground.setVisibility(View.INVISIBLE);
                validate_manual_ticket_info.setVisibility(View.INVISIBLE);
                backgroundManualValid.setVisibility(View.INVISIBLE);
                enableInput();
                txtCodeBox1.setText("");
                txtCodeBox2.setText("");
                txtCodeBox3.setText("");
                txtCodeBox4.setText("");
                txtCodeBox5.setText("");
                txtCodeBox6.setText("");
                txtCodeBox7.setText("");
                txtCodeBox8.setText("");
                txtCodeBox9.setText("");
                txtCodeBox1.requestFocus();
                txt_validate_status_prompt.setText("MANUAL VALIDATION");
                txt_validate_status_prompt.setTextColor(Color.parseColor("#FFFFFF"));

            }
        });



        txt_validate_status_prompt = (TextView)findViewById(R.id.txt_validate_status_prompt);

        txtManualScannedDate = (TextView)findViewById(R.id.txtManualScannedDate);
        txtManualScannedTime = (TextView)findViewById(R.id.txtManualScannedTime);

        txtManualUsedOrderId = (TextView)findViewById(R.id.txtManualUsedOrderId);

        usedTicketInfo = (LinearLayout)findViewById(R.id.manual_used_ticket_info);
        usedTicketInfo.setVisibility(View.INVISIBLE);

        resultManualImageLayer = (LinearLayout)findViewById(R.id.resultManualImageLayer);
        resultManualImageLayer.setVisibility(View.INVISIBLE);

        mvbackground = (LinearLayout)findViewById(R.id.mvbackground);
        mvbackground.setVisibility(View.INVISIBLE);

        manual_scan_failed_info = (LinearLayout)findViewById(R.id.manual_scan_failed_info);
        manual_scan_failed_info.setVisibility(View.INVISIBLE);


        txtCodeBox1 = (EditText)findViewById(R.id.txtCodeBox1);
        txtCodeBox2 = (EditText)findViewById(R.id.txtCodeBox2);
        txtCodeBox3 = (EditText)findViewById(R.id.txtCodeBox3);
        txtCodeBox4 = (EditText)findViewById(R.id.txtCodeBox4);
        txtCodeBox5 = (EditText)findViewById(R.id.txtCodeBox5);
        txtCodeBox6 = (EditText)findViewById(R.id.txtCodeBox6);
        txtCodeBox7 = (EditText)findViewById(R.id.txtCodeBox7);
        txtCodeBox8 = (EditText)findViewById(R.id.txtCodeBox8);
        txtCodeBox9 = (EditText)findViewById(R.id.txtCodeBox9);

        btnSearch = (Button)findViewById(R.id.btnSearch);
        btnSearch.requestFocus();
        if (RFIDCheck.equals("true")){
            btnSearch.setText("SAVE");
        }
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((txtCodeBox1.getText().length() != 0)&&(txtCodeBox2.getText().length() != 0)&&(txtCodeBox3.getText().length() != 0)&&(txtCodeBox4.getText().length() != 0)&&(txtCodeBox5.getText().length() != 0)&&(txtCodeBox6.getText().length() != 0)&&(txtCodeBox7.getText().length() != 0)&&(txtCodeBox8.getText().length() != 0)&&(txtCodeBox9.getText().length() != 0)) {

                    if (RFIDCheck.equals("true")){
                        String ticketCode;
                        if (txtrfid.getText().toString().length() < 9) {
                            return;
                        }
                        ticketCode = txtCodeBox1.getText().toString() + txtCodeBox2.getText().toString() + txtCodeBox3.getText().toString() + "-" + txtCodeBox4.getText().toString() + txtCodeBox5.getText().toString() + txtCodeBox6.getText().toString() + "-" + txtCodeBox7.getText().toString() + txtCodeBox8.getText().toString() + txtCodeBox9.getText().toString();
                        OkHttpClient client = new OkHttpClient();
                        code = ticketCode;
                        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
                        RequestBody body = RequestBody.create(mediaType, "code="+ticketCode+"&rfid="+txtrfid.getText().toString()+"&event_id=97&token="+myToken);
                        Request request = new Request.Builder()
                                .url("http://192.168.2.2:8000/api/laravel/rfid/accounts/check")
                                .post(body)
                                .addHeader("content-type", "application/x-www-form-urlencoded")
                                .addHeader("cache-control", "no-cache")
                                .build();

                        cpbLoginProgress.setIndeterminate(true);
                        btnSearch.setEnabled(false);

                        client.newCall(request).enqueue(new Callback() {
                            @Override
                            public void onFailure(Call call, IOException e) {

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        cpbLoginProgress.setIndeterminate(false);

                                    }
                                });



                            }

                            @Override
                            public void onResponse(Call call, Response response) throws IOException {
                                final String in[] = new String[9];
                                SimpleDateFormat myDateFormat = new SimpleDateFormat("MMM dd, yyyy");
                                SimpleDateFormat myTimeFormat = new SimpleDateFormat("HH:mm");
                                Date now = new Date();
                                scannedDate = myDateFormat.format(now);
                                scannedTime = myTimeFormat.format(now);
                                String status =  "";
                                String jsonData = response.body().string().toString();

                                try {
                                    JSONObject Jobject = new JSONObject(jsonData); //qr_status
                                    status =  Jobject.getString("qr_status").toString();
                                    Log.i("ManualValidationAC",jsonData);
                                    in[0] =  "QR status: "+status;
                                    in[1] =  "RFID status: "+Jobject.getString("qr_rfid_status").toString();
                                    in[2] =  "Buyer name: "+Jobject.getString("buyer_name").toString();
                                    in[3] =  "QR code: "+Jobject.getString("qr_code").toString();
                                    in[4] =  "Time: "+Jobject.getString("bought_time").toString();
                                    in[5] =  "Ticket type: "+Jobject.getString("ticket_name").toString();
                                    in[6] =  "Balance: "+Jobject.getString("balance").toString();
                                    in[7] =  "Order_id: "+Jobject.getString("order_id").toString();
                                    in[8] =  "RFID code: "+Jobject.getString("rfid").toString();
                                    detailrfid = in[0] + " / " + in[1] + " / " +in[2] + " / "+in[3] + " / " + in[4] + " / " + in[5] + " / " + in[6] + " / " + in[7] + " / " + in[8];
                                    if(status.contains("0") || status.contains("3")) {
                                        scan_status = "Ticket Not Found";
                                        Log.d("GateAgent", scan_status);
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                cpbLoginProgress.setIndeterminate(false);
                                                btnSearch.setEnabled(true);

                                            }
                                        });

                                        // Update UI
                                        txt_validate_status_prompt.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                preventInput();
                                                Map<String, Integer> map = new HashMap<String, Integer>();
                                                map.put("cross", R.drawable.crossmark);
                                                imageManualValidateResult.setImageResource(map.get("cross"));
                                                txt_validate_status_prompt.setText("VALIDATION FAILED");
                                                txt_validate_status_prompt.setTextColor(Color.parseColor("#FF0000"));
                                                manual_scan_failed_info.setVisibility(View.VISIBLE);
                                                resultManualImageLayer.setVisibility(View.VISIBLE);
                                            }
                                        });
                                        // Record data
                                        Ticket ticket = new Ticket(code, txtrfid.getText().toString(), scannedTime, "Failed", scannedDate, "NA", sessionData.getUsername(), sessionData.getEvent_name());
                                        DatabaseHandler db = new DatabaseHandler(ManualValidationActivity.this.getApplicationContext());
                                        db.addTicket(ticket);
                                        db.close();

                                        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                resultManualImageLayer.setVisibility(View.INVISIBLE);
                                                mvbackground.setVisibility(View.VISIBLE);
                                                mvbackground.bringToFront();
                                            }
                                        }, 3000);

                                    }  else if(status.contains("1")) {

                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                cpbLoginProgress.setIndeterminate(false);
                                                btnSearch.setEnabled(true);
                                                new AlertDialog.Builder(ManualValidationActivity.this)
                                                        .setTitle("RFID")
                                                        .setMessage(detailrfid)
                                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int which) {
                                                                // continue with delete
                                                                dialog.dismiss();
                                                            }
                                                        })
                                                        .setIcon(android.R.drawable.ic_dialog_info)
                                                        .show();
//                                                MaterialDialog.Builder builder = new MaterialDialog.Builder(ManualValidationActivity.this)
//                                                        .title("Test")
//                                                        .items(in)
//                                                        .theme(Theme.LIGHT)
//                                                        .positiveText("OK")
//                                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
//                                                            @Override
//                                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                                                                // TODO
//                                                                dialog.dismiss();
//                                                            }
//                                                        })
//                                                        .autoDismiss(false)
//                                                        .itemsCallback(new MaterialDialog.ListCallback() {
//                                                            @Override
//                                                            public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
//
//
//                                                            }
//                                                        });
//                                                MaterialDialog dialog = builder.build();
//                                                dialog.show();


                                            }
                                        });

                                        Log.d("GateAgent", "Ticket Scan Success");
                                        // String scanned_datetime = Jobject.getString("verify_at");
                                        order_id = Jobject.getString("order_id");
                                        // verify_at = Jobject.getString("verify_at");
                                        fName = "";//Jobject.getString("buyer_name");
                                        lName = Jobject.getString("buyer_name");
                                        //verified_time = verify_at.substring(0, 5);
                                        // verified_date = verify_at.substring(6);
                                        purchased_datetime = Jobject.getString("bought_time");
                                        ticket_type = Jobject.getString("ticket_name");


                                        // Update UI
                                        txtManualTicketType.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                preventInput();
                                                Map<String, Integer> map = new HashMap<String, Integer>();
                                                map.put("checkmark", R.drawable.checkmark);
                                                imageManualValidateResult.setImageResource(map.get("checkmark"));
                                                txtMySeatManual.setText("-");
                                                txtManualTicketType.setText(ticket_type);
                                                txtManualFirstName.setText(fName);
                                                txtManualLastName.setText(lName);
                                                txtManualQRCode.setText("Entrant Code: " + code);
                                                txtManualTicketBought.setText("Ticket Bought: " + purchased_datetime);
                                                txtManualOrderId.setText("Order ID: " + order_id);
                                                validate_manual_ticket_info.setVisibility(View.VISIBLE);
                                                txt_validate_status_prompt.setText("VALIDATION SUCCESS");
                                                txt_validate_status_prompt.setTextColor(Color.parseColor("#48C8F3"));
                                                resultManualImageLayer.setVisibility(View.VISIBLE);

                                                // String code, String type, String time, String status, String date_scanned, String orderNumber, String agentUser, String eventName
                                                Ticket ticket = new Ticket(code, ticket_type, scannedTime, "S"+txtrfid.getText().toString(), scannedDate, order_id, sessionData.getUsername(), sessionData.getEvent_name());
                                                DatabaseHandler db = new DatabaseHandler(ManualValidationActivity.this.getApplicationContext());
                                                db.addTicket(ticket);
                                                db.close();

                                                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        Log.v("GateAgent", "Displaying Valid Ticket Background");

                                                        // used_ticket_info.setVisibility(View.INVISIBLE);
                                                        // scan_failed_info.setVisibility(View.VISIBLE);
                                                        backgroundManualValid.setVisibility(View.VISIBLE);
                                                        resultManualImageLayer.setVisibility(View.INVISIBLE);


                                                    }
                                                }, 3000);

                                            }
                                        });
                                    }else if(status.contains("2") ){
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                cpbLoginProgress.setIndeterminate(false);
                                                btnSearch.setEnabled(true);

                                                new AlertDialog.Builder(ManualValidationActivity.this)
                                                        .setTitle("RFID")
                                                        .setMessage(detailrfid)
                                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int which) {
                                                                // continue with delete
                                                                dialog.dismiss();
                                                            }
                                                        })
                                                        .setIcon(android.R.drawable.ic_dialog_info)
                                                        .show();


                                            }
                                        });




                                        Log.d("GateAgent", "Ticket Scan Success");
                                        // String scanned_datetime = Jobject.getString("verify_at");
                                        order_id = Jobject.getString("order_id");
                                        // verify_at = Jobject.getString("verify_at");
                                        fName = ""; //Jobject.getString("buyer_name");
                                        lName = Jobject.getString("buyer_name");
                                        //  verified_time = verify_at.substring(0, 5);
                                        // verified_date = verify_at.substring(6);
                                        purchased_datetime = Jobject.getString("bought_time");
                                        ticket_type = Jobject.getString("ticket_name");


                                        // Update UI
                                        txtManualTicketType.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                preventInput();
                                                Map<String, Integer> map = new HashMap<String, Integer>();
                                                map.put("checkmark", R.drawable.checkmark);
                                                imageManualValidateResult.setImageResource(map.get("checkmark"));
                                                txtMySeatManual.setText("-");
                                                txtManualTicketType.setText(ticket_type);
                                                txtManualFirstName.setText(fName);
                                                txtManualLastName.setText(lName);
                                                txtManualQRCode.setText("Entrant Code: " + code);
                                                txtManualTicketBought.setText("Ticket Bought: " + purchased_datetime);
                                                txtManualOrderId.setText("Order ID: " + order_id);
                                                validate_manual_ticket_info.setVisibility(View.VISIBLE);
                                                txt_validate_status_prompt.setText("VALIDATION SUCCESS");
                                                txt_validate_status_prompt.setTextColor(Color.parseColor("#48C8F3"));
                                                resultManualImageLayer.setVisibility(View.VISIBLE);

                                                // String code, String type, String time, String status, String date_scanned, String orderNumber, String agentUser, String eventName
                                                Ticket ticket = new Ticket(code, ticket_type, scannedTime, "S"+txtrfid.getText().toString(), scannedDate, order_id, sessionData.getUsername(), sessionData.getEvent_name());
                                                DatabaseHandler db = new DatabaseHandler(ManualValidationActivity.this.getApplicationContext());
                                                db.addTicket(ticket);
                                                db.close();

                                                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        Log.v("GateAgent", "Displaying Valid Ticket Background");

                                                        // used_ticket_info.setVisibility(View.INVISIBLE);
                                                        // scan_failed_info.setVisibility(View.VISIBLE);
                                                        backgroundManualValid.setVisibility(View.VISIBLE);
                                                        resultManualImageLayer.setVisibility(View.INVISIBLE);


                                                    }
                                                }, 3000);

                                            }
                                        });
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }




                            }


                        });



                    }else{

                        String ticketCode;
                        ticketCode = txtCodeBox1.getText().toString() + txtCodeBox2.getText().toString() + txtCodeBox3.getText().toString() + "-" + txtCodeBox4.getText().toString() + txtCodeBox5.getText().toString() + txtCodeBox6.getText().toString() + "-" + txtCodeBox7.getText().toString() + txtCodeBox8.getText().toString() + txtCodeBox9.getText().toString();
                        validateCode(ticketCode);
                        // ((InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
                        txtCodeBox9.onEditorAction(EditorInfo.IME_ACTION_DONE);

                    }



                }

            }
        });

        txtCodeBox1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() == 0) {

                } else {
                    txtCodeBox2.requestFocus();
                }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        txtCodeBox2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() == 0) {
                    txtCodeBox1.requestFocus();
                } else {
                    txtCodeBox3.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        txtCodeBox3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() == 0) {
                    txtCodeBox2.requestFocus();
                } else {
                    txtCodeBox4.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        txtCodeBox4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() == 0) {
                    txtCodeBox3.requestFocus();
                } else {
                    txtCodeBox5.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        txtCodeBox5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.length() == 0) {
                    txtCodeBox4.requestFocus();
                } else {
                    txtCodeBox6.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        txtCodeBox6.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.length() == 0) {
                    txtCodeBox5.requestFocus();
                } else {
                    txtCodeBox7.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        txtCodeBox7.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.length() == 0) {
                    txtCodeBox6.requestFocus();
                } else {
                    txtCodeBox8.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        txtCodeBox8.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() == 0) {
                    txtCodeBox7.requestFocus();
                } else {
                    txtCodeBox9.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        txtCodeBox9.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.length() == 0) {
                    txtCodeBox8.requestFocus();
                } else {

                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.v("GateAgent", txtCodeBox9.getText().toString());
            }
        });
    }

    private void clearInput() {
        txtCodeBox1.setText("");
        txtCodeBox2.setText("");
        txtCodeBox3.setText("");
        txtCodeBox4.setText("");
        txtCodeBox5.setText("");
        txtCodeBox6.setText("");
        txtCodeBox7.setText("");
        txtCodeBox8.setText("");
        txtCodeBox9.setText("");
    }

    private void preventInput() {
        txtCodeBox1.setEnabled(false);
        txtCodeBox2.setEnabled(false);
        txtCodeBox3.setEnabled(false);
        txtCodeBox4.setEnabled(false);
        txtCodeBox5.setEnabled(false);
        txtCodeBox6.setEnabled(false);
        txtCodeBox7.setEnabled(false);
        txtCodeBox8.setEnabled(false);
        txtCodeBox9.setEnabled(false);
    }

    private void enableInput() {
        txtCodeBox1.setEnabled(true);
        txtCodeBox2.setEnabled(true);
        txtCodeBox3.setEnabled(true);
        txtCodeBox4.setEnabled(true);
        txtCodeBox5.setEnabled(true);
        txtCodeBox6.setEnabled(true);
        txtCodeBox7.setEnabled(true);
        txtCodeBox8.setEnabled(true);
        txtCodeBox9.setEnabled(true);
    }

    private void validateCode(String ticketCode) {
        code = ticketCode;
        final String urlParameter = "token="+myToken+"&code="+code;

        new ValidateTask(cpbLoginProgress).execute(urlParameter);
    }

    private class ValidateTask extends AsyncTask<String, Void, String> {

        CircularProgressBar cpbLoginProgress;

        public ValidateTask(CircularProgressBar cpbLoginProgress) {
            this.cpbLoginProgress = cpbLoginProgress;

        }

        protected void onPreExecute() {
            cpbLoginProgress.setIndeterminate(true);
        }

        @Override
        protected String doInBackground(String... params) {
            int responseCode = 0;
            StringBuilder responseOutput = new StringBuilder("");

            try {
                URL url = new URL("https://www.ticketmelon.com/api/codes/verify");
                HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                String urlParameters = params[0];
                connection.setRequestMethod("POST");
                connection.setRequestProperty("USER-AGENT", "Mozilla/5.0");
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestProperty("Accept", "application/json");
                connection.setDoOutput(true);
                DataOutputStream dStream = new DataOutputStream(connection.getOutputStream());
                dStream.writeBytes(urlParameters);
                dStream.flush();
                dStream.close();
                responseCode = connection.getResponseCode();
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line = "";
                responseOutput = new StringBuilder();
                while((line = br.readLine()) != null){
                    responseOutput.append(line);
                }
                br.close();

            } catch(MalformedURLException e) {

            }catch (Exception e) {

            }
            return responseOutput.toString();
        }



        protected void onPostExecute(String result) {
            cpbLoginProgress.setIndeterminate(false);

            Log.d("GateAgent", result.toString());

            SimpleDateFormat myDateFormat = new SimpleDateFormat("MMM dd, yyyy");
            SimpleDateFormat myTimeFormat = new SimpleDateFormat("HH:mm");
            Date now = new Date();
            scannedDate = myDateFormat.format(now);
            scannedTime = myTimeFormat.format(now);

            try {
                JSONObject object = new JSONObject(result);

                msg = object.getString("msg");

                if(msg.contains("Ticket Not Found")) {
                    scan_status = "Ticket Not Found";
                    Log.d("GateAgent", scan_status);


                    // Update UI
                    txt_validate_status_prompt.post(new Runnable() {
                        @Override
                        public void run() {
                            preventInput();
                            Map<String, Integer> map = new HashMap<String, Integer>();
                            map.put("cross", R.drawable.crossmark);
                            imageManualValidateResult.setImageResource(map.get("cross"));
                            txt_validate_status_prompt.setText("VALIDATION FAILED");
                            txt_validate_status_prompt.setTextColor(Color.parseColor("#FF0000"));
                            manual_scan_failed_info.setVisibility(View.VISIBLE);
                            resultManualImageLayer.setVisibility(View.VISIBLE);
                        }
                    });
                    // Record data
                    Ticket ticket = new Ticket(code, "NA", scannedTime, "Failed", scannedDate, "NA", sessionData.getUsername(), sessionData.getEvent_name());
                    DatabaseHandler db = new DatabaseHandler(ManualValidationActivity.this.getApplicationContext());
                    db.addTicket(ticket);
                    db.close();

                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            resultManualImageLayer.setVisibility(View.INVISIBLE);
                            mvbackground.setVisibility(View.VISIBLE);
                            mvbackground.bringToFront();
                        }
                    }, 3000);

                } else if(msg.contains("is already verified")) {
                    scan_status = "Ticket Already Verified";
                    Log.d("GateAgent", scan_status);
                    String scanned_datetime = object.getString("verify_at");
                    order_id = object.getString("order_id");
                    verify_at = object.getString("verify_at");
                    verified_time = verify_at.substring(0, 5);
                    verified_date = verify_at.substring(6);
                    ticket_type = object.getString("ticket_type");
                    Log.d("GateAgent", "Verified at : " + scanned_datetime);

                    // Update UI
                    txt_validate_status_prompt.post(new Runnable() {
                        @Override
                        public void run() {
                            preventInput();
                            Map<String, Integer> map = new HashMap<String, Integer>();
                            map.put("cross", R.drawable.crossmark);
                            imageManualValidateResult.setImageResource(map.get("cross"));
                            txt_validate_status_prompt.setText("VALIDATION FAILED");
                            txt_validate_status_prompt.setTextColor(Color.parseColor("#FF0000"));
                            txtManualScannedTime.setText(verified_time);
                            txtManualScannedDate.setText(verified_date);
                            txtManualUsedOrderId.setText("Order ID: " + order_id);
                            usedTicketInfo.setVisibility(View.VISIBLE);
                            resultManualImageLayer.setVisibility(View.VISIBLE);

                            // Record Data

                            // String code, String type, String time, String status, String date_scanned, String orderNumber, String agentUser, String eventName
                            Ticket ticket = new Ticket(code, ticket_type, scannedTime, "Used", scannedDate, order_id, sessionData.getUsername(), sessionData.getEvent_name());
                            DatabaseHandler db = new DatabaseHandler(ManualValidationActivity.this.getApplicationContext());
                            db.addTicket(ticket);
                            db.close();

                            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    resultManualImageLayer.setVisibility(View.INVISIBLE);
                                    mvbackground.setVisibility(View.VISIBLE);
                                    mvbackground.bringToFront();
                                }
                            }, 3000);

                        }
                    });


                } else if(msg.isEmpty()) {
                    Log.d("GateAgent", "Ticket Scan Success");
                    String scanned_datetime = object.getString("verify_at");
                    order_id = object.getString("order_id");
                    verify_at = object.getString("verify_at");
                    fName = object.getString("buyer");
                    lName = object.getString("buyer_lastname");
                    verified_time = verify_at.substring(0, 5);
                    verified_date = verify_at.substring(6);
                    purchased_datetime = object.getString("purchased_at");
                    ticket_type = object.getString("ticket_type");


                    JSONObject myObj = (JSONObject)object.getJSONArray("seat").get(0);
                    seatNumber = myObj.getString("number");
                    seatRow = myObj.getString("row");
                    seatSection = myObj.getString("section");
                    mySeat = seatSection + "  /  " + seatRow + "  /  " + seatNumber;
                    Log.d("GateAgent", "Seat: " + mySeat);



                    Log.d("GateAgent", "Ticket Type: " + ticket_type);
                    Log.d("GateAgent", "Order ID: " + order_id);
                    Log.d("GateAgent", "First Name: " + fName);
                    Log.d("GateAgent", "Last Name: " + lName);
                    Log.d("GateAgent", "Verified at : " + scanned_datetime);

                    // Update UI
                    txtManualTicketType.post(new Runnable() {
                        @Override
                        public void run() {
                            preventInput();
                            Map<String, Integer> map = new HashMap<String, Integer>();
                            map.put("checkmark", R.drawable.checkmark);
                            imageManualValidateResult.setImageResource(map.get("checkmark"));
                            txtMySeatManual.setText(mySeat);
                            txtManualTicketType.setText(ticket_type);
                            txtManualFirstName.setText(fName);
                            txtManualLastName.setText(lName);
                            txtManualQRCode.setText("Entrant Code: " + code);
                            txtManualTicketBought.setText("Ticket Bought: " + purchased_datetime);
                            txtManualOrderId.setText("Order ID: " + order_id);
                            validate_manual_ticket_info.setVisibility(View.VISIBLE);
                            txt_validate_status_prompt.setText("VALIDATION SUCCESS");
                            txt_validate_status_prompt.setTextColor(Color.parseColor("#48C8F3"));
                            resultManualImageLayer.setVisibility(View.VISIBLE);

                            // String code, String type, String time, String status, String date_scanned, String orderNumber, String agentUser, String eventName
                            Ticket ticket = new Ticket(code, ticket_type, scannedTime, "Success", scannedDate, order_id, sessionData.getUsername(), sessionData.getEvent_name());
                            DatabaseHandler db = new DatabaseHandler(ManualValidationActivity.this.getApplicationContext());
                            db.addTicket(ticket);
                            db.close();

                            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Log.v("GateAgent", "Displaying Valid Ticket Background");

                                    // used_ticket_info.setVisibility(View.INVISIBLE);
                                    // scan_failed_info.setVisibility(View.VISIBLE);
                                    backgroundManualValid.setVisibility(View.VISIBLE);
                                    resultManualImageLayer.setVisibility(View.INVISIBLE);


                                }
                            }, 3000);

                        }
                    });
                }







/*                txt_validate_status_prompt.post(new Runnable() {
                    @Override
                    public void run() {

                        Map<String, Integer> map = new HashMap<String, Integer>();
                        if(msg.contains("already verified")) {
                            map.put("cross", R.drawable.crossmark);
                            imageManualValidateResult.setImageResource(map.get("cross"));
                            txt_validate_status_prompt.setText("VALIDATION FAILED");
                            txt_validate_status_prompt.setTextColor(Color.parseColor("#FF0000"));
                            txtManualScannedTime.setText(verified_time);
                            txtManualScannedDate.setText(verified_date);
                            txtManualUsedOrderId.setText("Order ID: " + order_id);
                            usedTicketInfo.setVisibility(View.VISIBLE);
                            resultManualImageLayer.setVisibility(View.VISIBLE);

                            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    resultManualImageLayer.setVisibility(View.INVISIBLE);
                                    mvbackground.setVisibility(View.VISIBLE);
                                    mvbackground.bringToFront();
                                }
                            }, 3000);
                        } else {
                            map.put("checkmark", R.drawable.checkmark);
                            imageManualValidateResult.setImageResource(map.get("checkmark"));
                            txt_validate_status_prompt.setText("VALIDATION SUCCESS");

                        }
                    }
                });*/



            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
