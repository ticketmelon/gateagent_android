package com.ticketmelon.gateagent;

import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Created by cube on 2/8/2016.
 */
public class MyTutorialFragment extends Fragment {


    // TODO: Rename and change types and number of parameters
    public static Fragment newInstance(int a) {
        MyTutorialFragment fragment = new MyTutorialFragment();
        Bundle args = new Bundle();
        args.putInt("drawableInt", a);
        fragment.setArguments(args);


        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.mytutorial_fragment, container, false);
        Bundle args = getArguments();
        int a = args.getInt("drawableInt");
        ImageView imageView = (ImageView)v.findViewById(R.id.insView1);
        imageView.setImageResource(a);
        return v;
    }
}
